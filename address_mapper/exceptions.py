class MalformedAddressError(Exception):
    """ Raised when an address cannot be parsed to extract a zip code."""

    def __init__(self, msg):
        self.message = ("Malformed Address: " + msg)

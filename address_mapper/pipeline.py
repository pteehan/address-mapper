
from apache_beam.io import WriteToText
import apache_beam as beam
import pandas as pd
from . import processing
import argparse
import csv
import logging

log_format = "%(asctime)s [%(levelname)-5.5s] %(name)s: %(message)s"
logging.basicConfig(level=logging.INFO, format=log_format)
logger = logging.getLogger(__name__)


def get_csv_headers():
    """Get the list of column headers for CSV output."""

    result = processing.process_address(processing.create_address(n=1)[0])
    return(",".join(result.keys()).encode())


def result_to_csv(result):
    """Convert result in dict to one line of CSV output."""

    df = pd.DataFrame(result, index=[0])
    df['full_address'] = df.full_address.str.replace("\n", "\\n")
    return(df.to_csv(header=False, index=False).encode())


def run(n_records=10000, output_prefix="output"):
    """Run the Apache beam pipeline."""

    logger.info("****** Address Mapper ******")
    logger.info("Running with n_records=" + str(n_records) +
                ", output_prefix=" + output_prefix)

    with beam.Pipeline("DirectRunner") as p:
        addr = p | beam.Create(processing.create_address(n_records))
        result = addr | beam.Map(processing.process_address)
        result_csv = result | beam.Map(result_to_csv)
        result_csv | beam.io.WriteToText(
            output_prefix, file_name_suffix='.csv',
            header=get_csv_headers() + b"\n",
            append_trailing_newlines=False)

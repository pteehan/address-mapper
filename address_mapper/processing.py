from faker import Faker
import logging
import eeweather
from eeweather.exceptions import UnrecognizedZCTAError
from .exceptions import MalformedAddressError
import pandas as pd
import numpy as np


logger = logging.getLogger(__name__)


def create_address(n=10000, seed=None):
    """Create n fake addresses as pipeline input."""

    logger.info("Creating " + str(n) + " addresses.")
    fake = Faker()
    fake.seed_instance(seed)
    return([fake.address() for i in range(n)])


def extract_zip(address):
    """Extract the zip code from an address.

    Assumes the zip code is at the end of the address, following a space.
    Raises MalformedAddressError if there are no spaces, or the zip
    is not five characters, or the zip cannot be coverted to int.
    """

    logger.debug("Extracting zip: " + address)
    address_parts = address.split(" ")
    if len(address_parts) == 0:
        raise MalformedAddressError("No spaces present in address: " + address)

    zip = address_parts[len(address_parts) - 1]

    try:
        int(zip)
    except ValueError:
        raise MalformedAddressError("Non-numeric zip: " + zip)
    if len(zip) != 5:
        raise MalformedAddressError("Zip is not length 5: " + zip)

    return(zip)


def get_weather_station(lat, long):
    """Find the closest weather station for a lat, long pair."""
    logger.debug("Mapping weather station: " + str(lat) + ", " + str(long))
    ranked_stations = eeweather.rank_stations(lat, long)
    station, warnings = eeweather.select_station(ranked_stations)
    return(station)


def process_address(address):
    """Process an address and return its lat, long, and weather station."""
    logger.debug("Processing address: " + address)
    lat, long = np.nan, np.nan
    station = None
    zip = None

    try:
        zip = extract_zip(address)
    except MalformedAddressError as e:
        logging.error(e.message)

    if zip is not None:
        try:
            lat, long = eeweather.zcta_to_lat_long(zip)
            station = str(get_weather_station(lat, long))
        except UnrecognizedZCTAError as e:
            #  invalid zip, this seems to be common with fake addresses
            logger.error(e.message)

    return({"full_address": address,
            "weather_station": station,
            "latitude": lat,
            "longitude": long})

import pytest
from address_mapper import pipeline
import pandas as pd
from glob import glob
import os

def test_full_pipeline():
    """Run the pipeline and test the output is correctly generated."""

    #cleanup existing tests
    existing_files = glob("test_file*.csv")
    [os.unlink(f) for f in existing_files]

    # run pipeline
    pipeline.run(n_records=8, output_prefix="test_file")

    # test output
    output_file = glob("test_file*.csv")[0]
    df = pd.read_csv(output_file)
    assert(len(df)==8)
    assert(set(df.columns)==set(['full_address', 'weather_station',
                                 'latitude', 'longitude']))

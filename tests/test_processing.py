import pytest
from address_mapper import processing, exceptions
import sys
import pandas as pd
import numpy as np

def test_addresses():
    """Test address creation."""
    address = processing.create_address(3, seed=0)
    # I had originally planned to test the content of address
    # But found that the random seed works differently on Python 2 and 3
    # Therefore it's not possible to deterministically generate test data
    assert(len(address)==3)

def test_zip():
    """Test zip code extraction."""

    addresses =['4876 Gallegos Vista Apt. 382\nLake Christine, VA 92929',
    '15781 Leslie Port Apt. 877\nGreenfort, AL 94834',
    '097 Cook Row Suite 139\nLake Douglasmouth, WY 67077']

    expected = ['92929', '94834', '67077']
    zips = [processing.extract_zip(a) for a in addresses]

    assert(zips==expected)

def test_zip_malformed():
    """Test bad addresses raise exceptions."""
    with pytest.raises(exceptions.MalformedAddressError):
        assert processing.extract_zip("ABCDE")
    with pytest.raises(exceptions.MalformedAddressError):
        assert processing.extract_zip("Foo Bar 1234567")
    with pytest.raises(exceptions.MalformedAddressError):
        assert processing.extract_zip("Foo Bar Baz")


def test_get_weather_station():
    """Test weather station ID is correctly returned."""

    assert str(processing.get_weather_station(0,0)) == '407780'
    assert str(processing.get_weather_station(35,-118)) == '723171'
    assert str(processing.get_weather_station(40,74)) == '692534'

def test_process_address():
    """Test complete address processing pipeline."""

    a = '4876 Gallegos Vista Apt. 382\nLake Christine, VA 92929'
    result = processing.process_address(a)
    assert result['full_address'] == a
    assert result['weather_station'] is None
    assert np.isnan(result['latitude'])
    assert np.isnan(result['longitude'])

    a = 'Somewhere CA 90210'
    result = processing.process_address(a)
    assert result['full_address'] == a
    assert result['weather_station']=='722885'
    assert round(result['latitude']) == 34
    assert round(result['longitude']) == -118

    a = 'Badaddress'
    result = processing.process_address(a)
    assert result['full_address'] == a
    assert result['weather_station'] is None
    assert np.isnan(result['latitude'])
    assert np.isnan(result['longitude'])

# Address Mapper

### Paul Teehan
### paul.teehan@gmail.com

`Address mapper` does the following using an Apache Beam pipeline:

- Generate fake addresses using [faker](https://github.com/joke2k/faker)
- Extract the zip code for each address
- Extract latitude and longitude from zip code using [eeweather](https://github.com/openeemeter/eeweather)
- Find the closest weather station using [eeweather](https://github.com/openeemeter/eeweather)
- Write the results as a CSV file with address, weather station ID, latitude,
and longitude.

# Installation

You can install directly from Gitlab as follows:

    pip install git+https://gitlab.com/pteehan/address-mapper.git

# Usage

There are two arguments:

- n_records (default 10000): The number of addresses to generate and map.
- output_prefix (default 'output'): The first part of the name of the
  output CSV file.

To run from the command line:

    address_mapper --n_records 100 --output_prefix my_file

To run inside Python:

    from address_mapper import pipeline
    pipeline.run(n_records=100, output_prefix="my_file")


# Notes

I generated the addresses in one batch and then created a PCollection.  
If you wanted to run the application with a lot of addresses (e.g.
which may not fit in memory), this could be rewritten, for example
by writing addresses to CSV files in one pipeline, and then reading in another.

Faker addresses seem to generate invalid zip codes quite frequently
(~70% of cases).  This is probably expected behavior, because there are 100K
possible 5-digit numbers and 32K valid ZCTAs according to [Wikipedia](https://en.wikipedia.org/wiki/ZIP_Code_Tabulation_Area).  
In these cases, eeweather raises an exception when looking
up lat/long.  I've retained these cases in the output and recorded Nulls
for weather station, latitude, and longitude.  To solve this problem in a real
application, I would try to geolocate on city and state as a fallback.  

I assumed that a valid zip code is 5 digits and numeric.  If the extracted
zip code doesn't fit this criteria, I return Nulls as above.  

According to Wikipedia there is not a one-to-one mapping between zip
code and ZCTA. I did not have time to investigate further and determine
how you could determine ZCTA from zip code.  So, for this application,
I used the zip code as the ZCTA.  

I've used default configuration for the Beam pipeline.  It would
be straightforward to add additional config parameters.  

I found that generating correct CSV output is surprisingly tricky if you
want to get the data types correct.  I'm using the default quoting behavior
which is to quote only fields with special characters, which means most
but not all addresses are quoted, while the other columns are not quoted.  
This means weather station ID will be interpreted as a numeric rather than
a string if the output is read by Pandas.  We could instead quote non-numeric
fields, but then numeric Nulls will be quoted as well, which could cause numeric
columns to be interpreted as strings if every value in a file is Null.  For
these reasons if data types are an issue I would use a file
format that has a schema embedded, such as Avro or Parquet.

"""Setup for the package."""
from setuptools import setup

setup(
    name='Address mapper',
    version='1.0.0',
    author='Paul Teehan',
    author_email='paul.teehan@gmail.com',
    description='Maps addresses to geolocation',
    packages=['address_mapper'],
    scripts=['bin/address_mapper'],
    install_requires=['apache_beam', 'faker', 'eeweather', 'pandas'],
)
